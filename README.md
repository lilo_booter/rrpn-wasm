# RRPN Web Assembly

Web assembly for the [rrpn] project.

This project is based on code from this article:

- https://aralroca.com/blog/first-steps-webassembly-rust

And heavily borrows xtermjs usage from the example provided at:

- https://xtermjs.org

## Execution

Compile with:

```
wasm-pack build --target web
```

Start with

```
npx serve .
```

And then open http://localhost:3000.

[rrpn]: https://gitlab.com/lilo_booter/rrpn
