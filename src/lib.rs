use wasm_bindgen::prelude::*;
use rrpn::stack::Stack;
use rrpn::parsers;
use rrpn::vocab;
use rrpn::io::Io;

#[wasm_bindgen(module="/io.js")]
extern "C" {
    #[wasm_bindgen]
    fn writeln(s: &str);
    #[wasm_bindgen]
    fn write(s: &str);
    #[wasm_bindgen]
    fn flush();
}

#[wasm_bindgen]
pub struct WebStack
{
    stack: Stack,
}

struct Webio { }

impl Webio {
    pub fn new( ) -> Self {
        Webio { }
    }
}

impl Io for Webio {
    fn println( &self, s: &str ) {
        writeln( s );
    }

    fn print( &self, s: &str ) {
        write( s );
    }

    fn flush( &self ) {
        flush( );
    }
}

#[wasm_bindgen]
impl WebStack {
    pub fn new() -> WebStack {
        WebStack {
            stack: Stack::new( ),
        }
    }

    pub fn teach( &mut self, width: u32, height: u32 ) {
        self.stack.io = Box::new( Webio::new( ) );
        vocab::teach( &mut self.stack );
        parsers::teach( &mut self.stack );
        let _ = self.stack.command( ": repl-ante ;" );
        let _ = self.stack.command( ": repl-post ;" );
        let _ = self.stack.command( ": repl-debug dump ;" );
        let _ = self.stack.command( &format!( ": t-width {} ;", width ) );
        let _ = self.stack.command( &format!( ": t-height {} ;", height ) );
    }

    pub fn run( &mut self, s: &str ) {
        if s != "" {
            self.exec("repl-ante");
            for input in s.split( "\n" ) {
                match self.stack.command(&input) {
                    Ok(_) => (),
                    Err(error) => self.stack.io.println(&format!( "ERR: {}", error ) ),
                }
            }
            self.exec("repl-post");
            if self.stack.depth() > 0 {
                self.exec("repl-debug");
            }
            self.stack.io.flush();
        }
    }

    pub fn exec(&mut self, token: &str) {
        if !self.stack.parsing() {
            let _ignore = self.stack.eval(token);
        }
    }
}
